/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable no-undef */
/* eslint-disable quotes */
const through 		= require('through2'),
	  fs 			= require('fs'),
	  jsonFormat 	= require('json-format');
	  log			= require('fancy-log'),
	  colors		= require('ansi-colors'),
	  PluginError 	= require('plugin-error');

const PLUGIN_NAME = 'gulp-ui5-version';

module.exports = function (options) {
	if (!(options instanceof Object)) {
		throw new PluginError(PLUGIN_NAME, 'Parameter not an object.');
	}

	function handleErr(errMsg) {
		errMsg.forEach(function (msg) {
			log.error(msg);
		});
		throw new PluginError(PLUGIN_NAME, 'Error in plugin ' + PLUGIN_NAME);
	}

	return through.obj(function (file, enc, cb) {
		if (file.isStream()) {
			throw new PluginError(PLUGIN_NAME, 'Steaming not supported');
		}

		if (file.isNull() || file.isDirectory()) {
			return cb(null, file);
		}

		if (file.isBuffer()) {
			const filePath = file.path;
			const fileName = filePath.match(/([\w\s-.]+)$/)[1];

			if (fileName.search(/^manifest.json$/) < 0) {
				log.error(fileName);
				handleErr(['Not a SAPUI5 manifest file']);
			}
			let fileContent = fs.readFileSync(filePath, {
				encoding: 'utf-8'
			});
			fileContent = JSON.parse(fileContent);
			let appVersion = fileContent['sap.app'].applicationVersion.version;
			let aVersion = appVersion.split('.');
			let oVersion = {
				major: Number(aVersion[0]),
				minor: Number(aVersion[1]),
				patch: Number(aVersion[2])
			};
			let sVersion;
			const typeMatch = options.type.match(/(patch|minor|major)$/);
			if(typeMatch) {
				const type = typeMatch[1];
				oVersion[type]++;
				oVersion.patch = type === "minor" || type === "major" ? 0 : oVersion.patch;
				oVersion.minor = type === "major" ? 0 : oVersion.minor;
				sVersion = `${oVersion.major}.${oVersion.minor}.${oVersion.patch}`;
				if (options.rc) {
					sVersion = `RC ${sVersion}`;
				}
			}
			else {
				const sTime = new Date().getTime();
				sVersion = `${oVersion.major}.${oVersion.minor}.${oVersion.patch}-B${sTime} (${options.type})`;
			}
			fileContent['sap.app'].applicationVersion.version = sVersion;
			fs.writeFileSync(filePath, jsonFormat(fileContent), {
				encoding: 'utf-8'
			});
			log(colors.green('Updated SAPUI5 application version from ' + appVersion + ' to ' + fileContent['sap.app'].applicationVersion.version + ' with type: ' + options.type));
		}
	});
};
