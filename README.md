# gulp-ui5-version

## Add as NPM module (recommended)

```sh
npm install --save-dev git+ssh://git@gitlab.com:yotsub/gulp-ui5-version.git
```

## Add as GIT submodule

Onto *gulpfile.js* repository, run commands:
```sh
git submodule add https://gitlab.com/yotsub/gulp-ui5-version.git
```

## How to implement

```js
const ui5version = require("gulp-ui5-version");
// For GIT submodule, add the relative path to the folder that contains the index.js
const pump = require("pump");
const { src } = require("gulp");

//#region VERSION

/**
 * @function increaseVersion
 * @description Increase the version
 * @param {string} sType The type of version increase
 * @param {function} cb The callback function
 */
const increaseVersion = (sType, sPath, cb) => {
	sPath = sPath ? (sPath.endsWith("/") ? sPath : sPath + "/") + "manifest.json" : "./WebContent/manifest.json";
	pump([
		src(sPath),
		ui5version({
			type: sType,
			rc: !!argv["release-candidate"] || !!argv["r"],
		}),
	], cb);
};

/**
 * @description Increase the application version using patch type (ie. the third level) (version: Major.Minor.Patch)
 */
function increaseVersionBuild(cb, argv) {
	increaseVersion("patch", argv, null, cb);
	return cb();
}

/**
 * @description Increase the application version using minor type (ie. the second level) (version: Major.Minor.Patch)
 */
function increaseVersionDeploy(cb, argv) {
	increaseVersion("minor", argv, null, cb);
	return cb();
}

/**
 * @description Increase the application version using major type (ie. the first level) (version: Major.Minor.Patch)
 */
function increaseVersionRelease(cb, argv) {
	increaseVersion("major", argv, null, cb);
	return cb();
}

//#endregion

exports.version = cb => {
	argv.major ? increaseVersionRelease(cb, argv) : argv.minor ? increaseVersionDeploy(cb, argv) : increaseVersionBuild(cb, argv);
};

```
